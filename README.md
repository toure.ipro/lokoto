# Lokoto project
---
## Context


Agence de location automobile :

LOKOTO, site marchand spécialisé dans la location de véhicules, propose sur son site divers véhicule disponible à la location. On retrouve sur ce site.
Diverses fonctionnalités en commençant par le catalogue/vitrine des véhicules dont les accès divergent selon la connexion ou non de l'utilisateur ainsi qu'une page de paiement et un planning de réservation. Les utilisateurs inconnus du site auront la possibilité de s'inscrire puis plus tard de se connecter comme les autres.
Les administrateurs du site pourront, eux, mettre à jour le catalogue via un formulaire. Les véhicules entrés présenteront diverses caractéristiques pour que l'utilisateur ait devant lui un maximum d'informations.



---
## Fonctionnalité

- Consultation voiture (hors-ligne)
- Connexion (utilisateur/admin)
- Réservation (utilisateur connectés)
- Véhicule (Catégorie, Marques, Modele, Type)
- Admin(gestion voiture, formulaire, import image etc)
- Panier et formulaire
- Etat de disponibilité des voitures


Equipe LOKOTO


## Commentaire : 

Première utilisation d'un framework.  
Séparation des differentes parties du projet en 50/50.  
Problème d'organisation au niveau du temps, le paiement n'a pas pu etre traiter ainsi que les categories.  
De plus, nous n'avons jamais fais de Javascript.


### Probleme Rencontré:

- Lors de l'utilisation de le commande "php artisan db:seed" on obtient un message d'erreur concernant la fonction randomDigit() mais elle fonctionne quand même.
- L'image de la voiture est sensé s'afficher sur la page d'accueil mais elle n'est pas presente. 
- La pagination fais n'importe quoi les blocs sont trop large et manque de style sinon tout est bon
- Probleme d'affichage pour l'ajout de voiture

Envoyer un message à #orga