<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;

class AdminsController extends Controller
{

    public function index(Request $request)
    {
        return view('admins.index')->withCars(Car::orderBy('created_at', 'DESC')->paginate(5));
    }
}