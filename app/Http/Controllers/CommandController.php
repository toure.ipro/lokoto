<?php

namespace App\Http\Controllers;

use App\Models\Command;
use App\Models\Car;
use DateTime;
use Illuminate\Http\Request;

class CommandController extends Controller
{
    //
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     */
    public function index(){

        //
        return view('commands.index')->with(['commands' => Command::all()]);
    }

    /**
     * @param Command $command
     * @return mixed
     *
     */

    public function show(Command $command){
        //
        return view('commands.show')->withCommand($command);

    }

    public function create($id){
        return view('commands.create')->withCar(Car::findOrFail($id));
    }

    public function store(Request $request){
        $this->validate($request, [
            'car_id' => 'required',
            'dateL' => 'required',
            'dateR' => 'required'
        ]);
        $car = Car::findOrFail($request->car_id);
        $dateLocation = new DateTime($request->dateL);
        $dateRetour = new DateTime($request->dateR);
        $jours = date_diff($dateLocation, $dateRetour);
        $prixTtc = $car->prixJ * $jours->format('%d');
        Command::create([
            'user_id' => auth()->user()->id,
            'car_id' => $request->car_id,
            'dateL' => $request->dateL,
            'dateR' => $request->dateR,
            'prixTTC' => $prixTtc,
        ]);
        $car->update([
            'dispo' => 0
        ]);
        return redirect()->route('users.profile', auth()->user()->id)->with([
            'success' => 'Commande envoyée'
        ]);
    }

}
