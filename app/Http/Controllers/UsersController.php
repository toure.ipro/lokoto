<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;

class UsersController extends Controller
{
    //
    public function Show($id)
    {
        return view('users.show')->withUser(User::findOrFail($id));    
    }


    public function login(){
        return view('users.login');
    }

    public function auth(Request $request){
        $this->validate($request, [
            'email' => 'required' ,
            'password' => 'required'
        ]);
        if(auth()->attempt(['email'=>$request->email, "password"=>$request->password])){
           return redirect()->route('cars.index');
        }
        else{
            return redirect()->route('users.login')->with([
                'error' => 'identifiant ou mot de passe incorrect'
            ]);
        }
    }

    public function logout(){
        auth()->logout();
        return redirect()->route('cars.index');
    }

    public function register(Request $request){

        $this->validate($request, [
            'email' => 'required' ,
            'password' => 'required',
            'name' => 'required',
            'tel' => 'required',
            'ville' => 'required',
        ]);
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'tel' => $request->tel,
            'ville' => $request->ville,
        ]);

        return redirect()->route('users.register')->with([
            'success' => 'Inscription réussie'
        ]);

    }

    public function create(){
        return view('users.register');
    }
}
