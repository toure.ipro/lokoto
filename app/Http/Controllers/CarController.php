<?php

namespace App\Http\Controllers;

use App\Models\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{

    public function index(Request $request)
    {

        if ($request->search !== null) {
            $cars = Car::orderBy('created_at', 'DESC')->whereMarque($request->search)->paginate(4);
            return view('cars.index')->with([
                'cars' => $cars,
                'title' => "Resultat(s) trouvé(s) pour : " . $request->search, 
                'count' => $cars->count()
            ]);
        } else {
            $cars = Car::all();
            return view('cars.index')->with([
                'cars' => Car::paginate(4),
                'title' => "Toutes les voitures : ",
                'count' => $cars->count()
            ]);
        }


        
        $cars = Car::all();
        $dispo = ['50', '100', '150', '200','250', '300'];
        foreach ($cars as $car){
            $car->prixJ = $dispo[rand(0,5)];
            $car->save();
        }
        
    }
    public function store(Request $request){
        //
        $this->validate($request, [
            'marque' => 'required',
            'model' => 'required',
            'type' => 'required',
            'prixJ' => 'required',
            'dispo' => 'required',
            'image' => 'required'
        ]);
        $name = '';
        if($request ->hasFile('image')){
            $file = $request->image;
            $name = $file->getClientOriginalName();
            $file->move(public_path('image'), $name);
        }
        Car::create([
            'marque' => $request->marque,
            'model' => $request->model,
            'type' => $request->type,
            'prixJ' => $request->prixJ,
            'dispo' => $request->dispo,
            'image' => '/image/' . $name
        ]);
        return redirect()->route('admins.index')->withSuccess('BattleCar ajoutée ');
    }

    /**
     * @param \App\Models\Car $car
     * @return \Illuminate\Http\Response
     *
     */
    public function show(Car $car){

        //
        return view('cars.show')->withCar($car);
    }


    /**
     * @param Car $car
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car){
        
        return view('cars.edit')->withCar($car);
    }


    /**
     * @param Request $request
     * @param Car $car
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car){
        
        $this->validate($request, [
            'marque' => 'required',
            'model' => 'required',
            'type' => 'required',
            'prixJ' => 'required',
            'dispo' => 'required',
        ]);
        $image = $car->image;
        if($request ->hasFile('image')){
            $file = $request->file('image');
            $name = $file->getClientOriginalName();
            $file->move(public_path('image'), $name);
            $image = '/image/'.$name;
        }
        $car->update([
            'marque' => $request->marque,
            'model' => $request->model,
            'type' => $request->type,
            'prixJ' => $request->prixJ,
            'dispo' => $request->dispo,
            'image' => $image
        ]);
        return redirect()->route('admins.index')->withSuccess('BattleCar modifié ');
    }

    
    /**
     * @param Car $car
     *
     * @return \Illuminate\Http\Response
     */

     public function destroy(Car $car){

        $car->delete();
        return redirect()->route('admins.index')->withSuccess('Voiture supprimé');
     }


}
