<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
          $this->call(AdminSeeder::class);
         \App\Models\User::factory()->count(30)->create();
         \App\Models\Car::factory()->count(30)->create();
         \App\Models\Command::factory()->count(30)->create();

    }
}
