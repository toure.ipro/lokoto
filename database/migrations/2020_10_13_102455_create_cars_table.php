<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('marque')->default("Dominus");
            $table->string('model')->default("2010");
            $table->string('type');
            $table->string('prixJ');
            $table->boolean('dispo')->default(0);
            $table->string('image')->default("https://rocket-league.com/content/media/items/avatar/220px/breakout/breakout-BurntSienna.png");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
