@extends('layouts.header')

@section('content')
    <div class="container">
        <div class="row my-5">
            <div class="col-md-8 mx-auto">
                <div class="card bg-light">
                    <h3 class="card-header">Modifier une voiture</h3>
                    <div class="card-body">
                        <form action="{{route('cars.update', $car->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                            {{method_field('put')}}
                            <div class="form-group">
                                <label for="">Marque*</label>
                            <input type="text" name="marque" id="" class="form-control" value="{{$car->marque}}">
                            </div>
                            <div class="form-group">
                                <label for="">Model*</label>
                                <input type="text" name="model" id="" class="form-control" value="{{$car->model}}">
                            </div>
                            <div class="form-group">
                                <label for="">Type*</label>
                                <select name="type" id="">
                                    <option value="" selected disabled> Choisissez une type </option>
                                    <option value="Diesel"{{ $car->type == 'Diesel' ? 'selected' : ''}}>Diesel</option>
                                    <option value="Essence" {{ $car->type == 'Essence' ? 'selected' : ''}}>Essence</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Prix Journalier*</label>
                                <input type="number" name="prixJ" id="" class="form-control" value="{{$car->prixJ}}">
                            </div>
                            <div class="form-group">
                                <label for="">Disponibilité*</label>
                                <select name="dispo" id="">
                                    <option value="" selected disabled> disponibilité voiture </option>
                                    <option value="1" {{ $car->dispo ? 'selected' : ''}}>Oui</option>
                                    <option value="0" {{ !$car->dispo ? 'selected' : ''}}>Non</option>
                                </select>
                            </div>
                            <div class="form-group">
                            <img src="{{$car->image}}" width="100" height="100" alt="" class="img-fluid">
                                <label for="">Image*</label>
                                <input type="file" name="image" id="" class="form-control" aria-describedby="helpId">
                            </div>
                            <button type="submit" class="btn btn-primary">Enregistrer modifications</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection