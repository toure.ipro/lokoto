@extends('layouts.header')

@section('content')

    <div class="col-my-4">
        

        <div class="col-md-8">
            <div class="card border border-black">    
                <h3 class="card-header">{{ $car->marque }}</h3>
                <div class="card-body">
                    <div class="car-img-top">
                        <img src="{{ $car->image }}" class="img-fluid rounded m-2" alt="">
                    </div>
                    <p class="d-flex flex-row justify-content-start">
                        <span class="badge badge-danger mx-3">Type: {{ $car->type }}</span>
                        <span class="badge badge-primary mr-3">Prix Journalier: {{ $car->prixJ }} €
                        </span>
                        @if ($car->dispo)
                            @auth
                                <p>
                                    <a href="{{route('command.create', $car->id)}}" class="btn btn-primary">Réserver</a>
                                </p>
                            @else
                                <p>
                                    <a href="{{route('users.login')}}" class="btn btn-primary">Réserver</a>
                                </p>
                            @endauth
                        @else
                            <span class="badge badge-warning">
                                Réservé
                            </span>
                        @endif
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
