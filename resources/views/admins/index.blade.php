@extends('layouts.header')

@section('content')
    <div class="row my-4">
        <div class="col-md-12">
            <div class="form-group">
                <button class="btn btn-primary" data-toggle="modal" data-target="#addCar">
                    Ajouter une voiture  <i class="fa fa-plus-square"></i>
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Marque</th>
                                <th>Modele</th>
                                <th>Type</th>
                                <th>Prix Journalier</th>
                                <th>Disponibilité</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cars as $car)
                                <tr>
                                    <td>{{$car->id}}</td>
                                    <td>{{$car->marque}}</td>
                                    <td>{{$car->model}}</td>
                                    <td>{{$car->type}}</td>
                                    <td>{{$car->prixJ}} €</td>
                                    <td>
                                        @if ($car->dispo)
                                            <span class="badge badge-success">
                                                Disponible à la location
                                            </span>
                                        @else
                                            <span class="badge badge-danger">
                                                Indisponible
                                            </span>
                                        @endif
                                    </td>
                                    <td>
                                        <img src="{{$car->image}}" width="150" height="150" class="img-fluid rounded" alt="">
                                    </td>
                                    <td>
                                    <a href="{{route('cars.edit', $car->id)}}" class="btn btn-warning" >Modifier</a>
                                    
                                    <form method="post" action="{{route('cars.destroy', $car->id)}}">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                        {!! $cars->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
<div class="modal fade" id="addCar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ajouter une BattleCar</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
            <div class="modal-body">
                    <form action="{{route('cars.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Marque*</label>
                            <input type="text" name="marque" id="" class="form-control" placeholder="Marque" aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                            <label for="">Model*</label>
                            <input type="text" name="model" id="" class="form-control" placeholder="Model" aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                            <label for="">Type*</label>
                            <select name="type" id="">
                                <option value="" selected disabled> Choisissez une type </option>
                                <option value="Diesel">Diesel</option>
                                <option value="Essence">Essence</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Prix Journalier*</label>
                            <input type="number" name="prixJ" id="" class="form-control" placeholder="Prix journalier" aria-describedby="helpId">
                        </div>
                        <div class="form-group">
                            <label for="">Disponibilité*</label>
                            <select name="dispo" id="">
                                <option value="" selected disabled> disponibilité voiture </option>
                                <option value="1">Oui</option>
                                <option value="0">Non</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Image*</label>
                            <input type="file" name="image" id="" class="form-control" aria-describedby="helpId">
                        </div>
                        <button type="submit" class="btn btn-primary">Enregistrer</button>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection