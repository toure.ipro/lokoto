@extends('layouts.header')

@section('content')


    <div class="row my-4">
        <div class="col-md-10 mx-auto">
            <div class="card border border-black shadow-sm">
                <h3 class="card-header">Réservation d'un véhicule</h3>
                <div class="row my-3">
                    <div class="col-md-12">
                            
                            <h3 class="text p-4">{{ $car->marque }}</h3>
                            <div class="card-body">
                                <div class="car-img-top">
                                    <img src="{{ $car->image }}" class="img-fluid rounded m-2" alt="">
                                </div>
                                <p class="d-flex flex-row justify-content-start">
                                    <span class="badge badge-danger mx-3">Type: {{ $car->type }}</span>
                                    <span class="badge badge-primary mr-3">Prix Journalier: {{ $car->prixJ }} €
                                    </span>
                                </p>
                            </div>
                        
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('commands.store') }}" >
                        @csrf
                        <div class="form-group">
                            <label >Date d'emprunt</label>
                            <input type="date" name="dateL" class="form-control" >
                        </div>

                        <div class="form-group">
                            <label >Date de Restitution</label>
                            <input type="date" name="dateR" class="form-control">
                        <input type="hidden" name="car_id" value="{{$car->id}}">
                        </div>

                        <div>
                            <button type="submit" class="btn border-secondary">Louer</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
