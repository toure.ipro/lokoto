@extends('layouts.header')

@section('content')


<div class="row my-4">
    <div class="col-md-5">
        <div class="card text-left">  
            <div class="card-body">
                <img class="card-img" src="{{$user->image}}" alt="">
                <h4 class="card-title">{{$user->name}}</h4>
                <p class="card-text d-flex flex-row align-items-center">
                    <span class="badge badge-primary mr-2">Téléphone : {{$user->tel}}</span>
                    <span class="badge badge-danger">Ville : {{$user->ville}}</span>
                </p>
            </div>
        </div>
    </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card card-body">
                <h3> Mes commandes :</h3>
            
                <table class="table-center">
                    <thead>
                        <tr>
                            <th>Marque</th>
                            <th>Type</th>
                            <th>Prix Journée</th>
                            <th>Date début</th>
                            <th>Date fin</th>
                            <th>Prix TTC</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (auth()->user()->commands as $command)
                            <tr>
                                <td>{{App\Models\Car::findOrFail($command->car_id)->marque}}</td>
                                <td>{{App\Models\Car::findOrFail($command->car_id)->type}}</td>
                                <td>{{App\Models\Car::findOrFail($command->car_id)->prixJ}}</td>
                                <td>{{$command->dateL}}</td>
                                <td>{{$command->dateR}}</td>
                                <td>{{$command->prixTTC}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
</div>

@endsection
