@extends('layouts.header')

@section('content')


    <div class="row my-4">
        <div class="col-md-8 mx-auto">
            <div class="card border border-grey shadow-sm">
                <h3 class="card-header">Inscription</h3>
                <div class="card-body">
                    <form method="post" action="{{ route('users.register') }}" >
                        @csrf
                        <div class="form-group">
                            <label for="name">Nom et Prenom</label>
                            <input type="text" name="name" class="form-control" placeholder="Nom Prenom">
                        </div>

                        <div class="form-group">
                            <label for="tel">Telephone</label>
                            <input type="tel" name="tel" class="form-control" placeholder="Numero">
                        </div>

                        <div class="form-group">
                            <label for="ville">Ville</label>
                            <input type="text" name="ville" class="form-control" placeholder="ville">
                        </div>

                        <div class="form-group">
                            <label for="email">Identifiant</label>
                            <input type="text" name="email" class="form-control" placeholder="email">
                        </div>

                        <div class="form-group">
                            <label for="password">Mot de Passe</label>
                            <input type="password" name="password" class="form-control" placeholder="Mot de Passe">
                        </div>

                        <div>
                            <button type="submit" class="btn border-secondary">S'inscrire</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    @endsection
