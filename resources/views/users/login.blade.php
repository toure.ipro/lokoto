@extends('layouts.header')

@section('content')


<div class="row my-4">
    <div class="col-md-8 mx-auto">
        <div class="card border border-grey shadow-sm">
            <h3 class="card-header">Connexion</h3>
            <div class="card-body">
                <form method="post" action="{{ route('users.auth') }}" >
                    @csrf
                    <div class="form-group">
                        <label for="email">Identifiant</label>
                        <input type="text" name="email" class="form-control" placeholder="email">
                    </div>

                    <div class="form-group">
                        <label for="email">Mot de Passe</label>
                        <input type="password" name="password" class="form-control" placeholder="Mot de Passe">
                    </div>

                    <div>
                        <button type="submit" class="btn border-secondary">Connexion</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

@endsection
